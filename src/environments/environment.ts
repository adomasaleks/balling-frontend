// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBe4RVq4ozNbXD1hq_l1GFnD4EGz1FI5BY',
    authDomain: 'balling-62cbc.firebaseapp.com',
    databaseURL: 'https://balling-62cbc.firebaseapp.com',
    projectId: 'balling-62cbc',
    storageBucket: 'gs://balling-62cbc.appspot.com',
    //messagingSenderId: '<your-messaging-sender-id>'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

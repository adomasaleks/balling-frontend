import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import {ModulesModule} from "./modules/modules.module";
import {CoreModule} from "./core/core.module";
import {AngularFireModule} from "@angular/fire";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "./app-routing.module";
import { AngularFireDatabaseModule } from "@angular/fire/database";

import {Globals} from "./shared/globals";

const config = {
  apiKey: "AIzaSyBe4RVq4ozNbXD1hq_l1GFnD4EGz1FI5BY",
  authDomain: "balling-62cbc.firebaseapp.com",
  databaseURL: "https://balling-62cbc.firebaseio.com",
  projectId: "balling-62cbc",
  storageBucket: "balling-62cbc.appspot.com",
  messagingSenderId: "1069905937043",
  appId: "1:1069905937043:web:f28e75fd7c806a8b"
}


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,

    RouterModule,
    AppRoutingModule,

    //Core
    CoreModule,

    //modules
    ModulesModule,

    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule,

  ],
  providers: [Globals],
  bootstrap: [AppComponent]
})
export class AppModule { }

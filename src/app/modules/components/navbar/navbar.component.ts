import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../core/services/auth.service';
import {Globals} from '../../../shared/globals';
import {Observable} from 'rxjs';
import {User} from '../../../core/models/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  user$: Observable<User>;

  constructor(private auth: AuthService, private globals: Globals) {}

  ngOnInit() {
    this.user$ = this.auth.user$;
  }

}

import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Inject} from "@angular/core";

@Component({
  selector: 'app-gamelist',
  templateUrl: './gamelist.component.html',
  styleUrls: ['./gamelist.component.sass']
})

export class GamelistComponent implements OnInit {
  panelOpenState = false;
  games;
  constructor(
    public dialogRef: MatDialogRef<GamelistComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  ngOnInit() {
    this.data.subscribe(games => {
      this.games = games;
    })
  }

}

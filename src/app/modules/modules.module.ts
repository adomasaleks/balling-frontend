import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  MatButtonModule,
  MatDialogModule,
  MatExpansionModule,
  MatInputModule,
  MatStepperModule,
  MatListModule, MatToolbarModule, MatIconModule
} from '@angular/material';
import { CreateProfileComponent } from "./pages/profile/create-profile/create-profile.component";
import { HomeComponent } from "./pages/home/home.component";
import { LoginComponent } from "./pages/login/login.component";
import { RegisterComponent } from "./pages/register/register.component";
import { MapComponent } from './pages/map/map.component';
import { AgmCoreModule } from '@agm/core';
import {RouterModule} from "@angular/router";
import { ProfileFormsComponent } from './forms/profile-forms/profile-forms.component';
import { EditProfileComponent } from './pages/profile/edit-profile/edit-profile.component';
import { GetProfileComponent } from './pages/profile/get-profile/get-profile.component';
import { GamelistComponent } from './components/gamelist/gamelist.component';
import { GameFormsComponent } from './forms/game-forms/game-forms.component';
import { CreateGameComponent } from './pages/games/create-game/create-game.component';
import { MineGamesComponent } from './pages/games/mine-games/mine-games.component';
import { OwnGameFormsComponent } from './forms/own-game-forms/own-game-forms.component';
import { NavbarComponent } from './components/navbar/navbar.component';


@NgModule({
  declarations: [
    CreateProfileComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    MapComponent,
    ProfileFormsComponent,
    EditProfileComponent,
    GetProfileComponent,
    GamelistComponent,
    GameFormsComponent,
    CreateGameComponent,
    MineGamesComponent,
    OwnGameFormsComponent,
    NavbarComponent,
  ],
  entryComponents: [
    OwnGameFormsComponent,
    GamelistComponent
  ],
  exports: [
    NavbarComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatExpansionModule,
    MatDialogModule,
    MatStepperModule,
    MatListModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCwfq_NnY-GPBhP1uKH1PYH-8GLNcB5KOE',
    }),
    MatToolbarModule,
    MatIconModule
  ]
})
export class ModulesModule { }

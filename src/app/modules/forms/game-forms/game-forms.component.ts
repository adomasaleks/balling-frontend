import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {GamesService} from "../../../core/services/games.service";
import {MatStepper} from "@angular/material";

@Component({
  selector: 'app-game-forms',
  templateUrl: './game-forms.component.html',
  styleUrls: ['./game-forms.component.sass']
})
export class GameFormsComponent implements OnInit {
  gameForm: FormGroup;
  lat: number;
  lng: number;
  markers;
  id;
  constructor(
    private fb: FormBuilder,
    public games: GamesService,
  ) {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      }, (err) => {
        this.lng = 54.687157;
        this.lat = 25.279652;
        console.log(err);
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  ngOnInit() {
    this.gameForm = this.fb.group({
      gameName: new FormControl(''),
      description: new FormControl(''),
      needed: new FormControl(''),
      playerCount: new FormControl(''),
    })
    this.games.getStadiums().then(stadiums => {
      stadiums.subscribe( data => {
        // @ts-ignore
        this.markers = data.locations;
      })
    })
  }
  setStadiumID (stepper: MatStepper, id) {
    this.id = id;
    stepper.next();
  }
  submit(e) {
    const game = {
      stadium_id: this.id,
      description: e.description,
      gameName: e.gameName,
      needed: e.needed,
      playerCount: e.playerCount
    }
    this.games.addGame(game);
  }
}

import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormArray} from '@angular/forms';
import {ProfileService} from "../../../core/services/profile.service";
import {CV} from "../../../core/models/CV.model";
import { Router } from "@angular/router";


@Component({
  selector: 'app-profile-forms',
  templateUrl: './profile-forms.component.html',
  styleUrls: ['./profile-forms.component.sass']
})
export class ProfileFormsComponent implements OnInit {
  profileForm: FormGroup;
  @Input() profile: CV = null;
  submitButton: string = "CREATE";
  constructor(
    public ps: ProfileService,
    private fb: FormBuilder,
    public router: Router
  ) { }
  ngOnInit() {
    this.profileForm = this.fb.group({
      height: new FormControl(''),
      weight: new FormControl(''),
      sports: new FormArray([this.createSports()]),
      favoriteSports: this.fb.array([this.fb.group({point: ''})]),
      sportsSchool: this.fb.array([this.fb.group({point: ''})]),
      tournaments: this.fb.array([this.createTournaments()]),
    });
    //console.log(this.profile);
    this.editProfile();
  }
  get favoriteSports() {
    return this.profileForm.get('favoriteSports') as FormArray;
  }
  get sportsSchool() {
    return this.profileForm.get('sportsSchool') as FormArray;
  }

  createTournaments(): FormGroup {
    return this.fb.group({
      name: '',
      place: '',
      trophy: '',
    })
  }
  createSports(): FormGroup {
    return this.fb.group({
      sport: '',
      position: '',
    });
  }
  addSport(): void {
    let items = this.profileForm.get('sports') as FormArray;
    items.push(this.createSports());
  }
  addTournaments(): void {
    let items = this.profileForm.get('tournaments') as FormArray;
    items.push(this.createTournaments());
  }
  addItem(item) {
    item.push(this.fb.group({point:''}));
  }
  editProfile() {
    if (this.profile) {
      this.submitButton = "UPDATE";
      for (var i = 1; i < Object.keys(this.profile.favoriteSports).length; i++)
        this.addItem(this.favoriteSports);
      for (var i = 1; i < Object.keys(this.profile.sportsSchool).length; i++)
        this.addItem(this.sportsSchool);
      for (var i = 1; i < Object.keys(this.profile.sports).length; i++)
        this.addSport();
      for (var i = 1; i < Object.keys(this.profile.tournaments).length; i++)
        this.addTournaments();
      this.profileForm.setValue(this.profile);
    } else {
      this.router.navigate(['/profile']);
    }
  }
  removeItem(item, index) {
    item.removeAt(index);
  }
}

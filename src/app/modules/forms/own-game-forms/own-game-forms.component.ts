import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {GamesService} from "../../../core/services/games.service";

@Component({
  selector: 'app-own-game-forms',
  templateUrl: './own-game-forms.component.html',
  styleUrls: ['./own-game-forms.component.sass']
})
export class OwnGameFormsComponent implements OnInit {
  ownGameForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    public game: GamesService,
    public dialogRef: MatDialogRef<OwnGameFormsComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
  ) {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.ownGameForm = this.fb.group({
      gameName: new FormControl(''),
      description: new FormControl(''),
      needed: new FormControl(''),
      playerCount: new FormControl(''),
    })

    this.game.getMineGame(this.data.uid).subscribe(data => {
      const editGame = {
        gameName: data.gameName,
        description: data.description,
        needed: data.needed,
        playerCount: data.playerCount
      }
      console.log(data);
      this.ownGameForm.setValue(editGame);
    })
  }

}

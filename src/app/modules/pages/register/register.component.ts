import { Component, OnInit } from '@angular/core';

import {AuthService} from "../../../core/services/auth.service";
import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { passwordValidator } from "../../../shared/validators";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;

  constructor(
    public auth: AuthService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      name: new FormControl('',
        Validators.required,
      ),
      lastname: new FormControl('',
        Validators.required,
      ),
      email: new FormControl('',
        Validators.required,
      ),
      password: new FormControl('',
        Validators.required,
      ),
      confirmPassword: new FormControl('', [
        Validators.required,
        passwordValidator
      ]),
    });

    //update confirm password value when password maches
    this.registerForm.controls.password.valueChanges
      .subscribe(
        x => this.registerForm.controls.confirmPassword.updateValueAndValidity()
      );
  }
  async submitRegister(){
    const formValue = this.registerForm.value;
    const displayName = formValue.name + ' ' + formValue.lastname;
    await this.auth.register(formValue.email, formValue.password, displayName);
  }
}

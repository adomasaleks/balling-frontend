import { Component, OnInit } from '@angular/core';
import {GameFormsComponent} from "../../../forms/game-forms/game-forms.component";

@Component({
  selector: 'app-create-game',
  templateUrl: './create-game.component.html',
  styleUrls: ['./create-game.component.sass']
})
export class CreateGameComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

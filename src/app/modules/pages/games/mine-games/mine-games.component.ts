import { Component, OnInit } from '@angular/core';
import {GamesService} from "../../../../core/services/games.service";
import {OwnGameFormsComponent} from "../../../forms/own-game-forms/own-game-forms.component";
import {MatDialog} from "@angular/material";
import {AngularFireAuth} from "@angular/fire/auth";

@Component({
  selector: 'app-mine-games',
  templateUrl: './mine-games.component.html',
  styleUrls: ['./mine-games.component.sass']
})
export class MineGamesComponent implements OnInit {

  minegame;
  uid;
  constructor(
    public games: GamesService,
    public dialog: MatDialog,
  ) {

  }
  ngOnInit() {
      this.games.getMineGames().subscribe(games => {
        this.minegame = games;
      });
  }

  openDialog(uid): void {
    this.dialog.open(OwnGameFormsComponent, {
      width: '250px',
      data: {uid: uid}
    });
  }
}

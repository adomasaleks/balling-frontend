import { Component, OnInit } from '@angular/core';
import { GamesService } from "../../../core/services/games.service";
import { GamelistComponent } from "../../components/gamelist/gamelist.component";
import {MatDialog} from '@angular/material/dialog';
import {MineGamesComponent} from "../games/mine-games/mine-games.component";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.sass']
})
export class MapComponent implements OnInit {

  lat: number;
  lng: number;
  fullImagePath = '../../assets/map/current_location.svg';
  markers;

  constructor(
    public games: GamesService,
    public dialog: MatDialog
  ) {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((position) => {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
      }, (err) => {
        this.lng = 54.687157;
        this.lat = 25.279652;
        console.log(err);
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }


  openGames(event): void {
    const dialogRef = this.dialog.open(GamelistComponent, {
      width: '80vw',
      data: this.games.getGames(event)
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  ngOnInit() {
    this.games.getStadiums().then((stadiums) => {
      stadiums
        .subscribe((data) => {
        // @ts-ignore
          this.markers = data.locations;
          console.log(typeof this.markers[0].latitude);
        })
    })
  }
}

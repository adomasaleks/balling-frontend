import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../../core/services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {ProfileService} from "../../../../core/services/profile.service";
import { CV } from "../../../../core/models/CV.model";
import { map } from "rxjs/operators";
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.sass']
})
export class EditProfileComponent implements OnInit {
  uid: string;
  profile: CV;
  constructor(
    public auth: AuthService,
    private route: ActivatedRoute,
    public ps: ProfileService,
  ) { }

  ngOnInit() {
    this.uid = this.route.snapshot.paramMap.get("uid");
    this.ps.getData(this.uid)
      .subscribe(params => {
      this.profile = params;
        console.log(this.profile);
    });
  }

}

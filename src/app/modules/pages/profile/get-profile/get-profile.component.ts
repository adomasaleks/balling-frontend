import { Component, OnInit } from '@angular/core';
import {ProfileService} from "../../../../core/services/profile.service";
import {ActivatedRoute} from "@angular/router";
import {Profile} from "../../../../core/models/profile.model";

@Component({
  selector: 'app-get-profile',
  templateUrl: './get-profile.component.html',
  styleUrls: ['./get-profile.component.sass']
})
export class GetProfileComponent implements OnInit {
  uid: string;
  profile: Profile;
  constructor(
    private ps: ProfileService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.uid = this.route.snapshot.paramMap.get("uid");
    this.ps.getData(this.uid).subscribe(params => {
      this.profile = params;
    });
  }

}

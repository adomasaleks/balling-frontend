import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateProfileComponent } from "./modules/pages/profile/create-profile/create-profile.component";
import { HomeComponent } from "./modules/pages/home/home.component";
import { AuthGuard } from "./core/guards/auth.guard";
import {LoginComponent} from "./modules/pages/login/login.component";
import {RegisterComponent} from "./modules/pages/register/register.component";
import {MapComponent} from "./modules/pages/map/map.component";
import {EditProfileComponent} from "./modules/pages/profile/edit-profile/edit-profile.component";
import {GetProfileComponent} from "./modules/pages/profile/get-profile/get-profile.component";
import {CreateGameComponent} from "./modules/pages/games/create-game/create-game.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'profile/edit/:uid', component: EditProfileComponent},
  {path: 'profile/:uid', component: GetProfileComponent},
  {path: 'profile', component: CreateProfileComponent, canActivate: [AuthGuard]},
  {path: 'map', component: MapComponent, canActivate: [AuthGuard]},
  {path: 'game', component: CreateGameComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export interface Stadium {
  lat: number;
  lng: number;
}

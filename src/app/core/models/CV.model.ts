export interface CV {
  weight: number;
  height: number;
  sports: {
    sport: string,
    position: string,
  };
  favoriteSports?: {
    point: string
  };
  sportsSchool?: {
    point:string
  };
  tournaments?: {
    name: string,
    place: number,
    trophy: string,
};
}

export interface game {
  stadium_id: string,
  description: string,
  gameName: string,
  needed: string,
  playerCount: string,
}

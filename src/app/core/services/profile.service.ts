import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';

import {AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { CV } from '../models/CV.model';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  $uid: string;
  profile: Observable<CV>;
  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) {
      // this.afAuth.authState.subscribe(user => {
      //   if(user) this.$uid = user.uid;
      // });
  }
  async addProfile(form){

    const userRef: AngularFirestoreDocument<CV> = this.afs.doc(`Users/${this.$uid}`);

    const data = {
      weight: form.weight,
      height: form.height,
      sports: form.sports,
      favoriteSports: form.favoriteSports,
      sportsSchool: form.sportsSchool,
      tournaments: form.tournaments,
    };
    return userRef.set(data, {merge: true});
  }

  getData(uid): Observable<CV> {
    return this.afs.doc<CV>(`Users/${uid}`)
      .valueChanges()
      .pipe(map((res) => {
        const {weight, height, sports, favoriteSports, sportsSchool, tournaments} = res;
        const form: CV = {
          weight,
          height,
          sportsSchool,
          sports,
          favoriteSports,
          tournaments
        };
        return form;
      }));
  }
}

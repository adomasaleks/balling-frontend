import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {Observable} from "rxjs";
import {AngularFireAuth} from "@angular/fire/auth";
import {map} from "rxjs/operators";
@Injectable({
  providedIn: 'root'
})
export class GamesService {
  uid: string;
  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth
  ) {
    this.afAuth.authState.subscribe(user => {
      if (user) this.uid = user.uid;
    })
  }
  getMineGames(): Observable<any>{
    return this.afs.collection("Games", ref => ref.where('gameMaster_id', '==', this.uid)).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }
  async getStadiums () {
    return this.afs.doc('Stadiums/Stadiums').valueChanges();
  }
  getGames (id): Observable<any> {
    return this.afs.collection("Games", ref => ref.where('stadium_id', '==', id)).valueChanges();
  }

  async addGame (game) {
    const setGame = this.afs.collection("Games").add(game);
    return setGame;
  }

  getMineGame(uid): Observable<any>{
    return this.afs.doc(`Games/${uid}`).valueChanges();
  }
}

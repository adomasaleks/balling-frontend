import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

import {AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import {BehaviorSubject, Observable, of} from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {User} from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  user$: Observable<User>;
  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private router: Router
  ) {
     this.user$ = this.afAuth.authState.pipe(
       switchMap(user => {
         if (user) {
           return this.afs.doc<User>(`Users/${user.uid}`).valueChanges();
         } else {
           return of(null);
         }
       })
     );
  }
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  async googleSignin() {
    const provider = new auth.GoogleAuthProvider();
    const credential = await this.afAuth.auth.signInWithPopup(provider);
    this.updateUserData(credential.user, "");
    return this.router.navigate(['map']);
  }
  async register(email: string, password: string, name: string) {
    const credential = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
    console.log("register" + credential.user.displayName);
    this.updateUserData(credential.user, name);
    return this.router.navigate(['map']);
    //this.sendEmailVerification();
  }
  async login(email: string, password: string){
    await this.afAuth.auth.signInWithEmailAndPassword(email, password);
    this.router.navigate(['map']);
  }
  async signOut() {
    this.loggedIn.next(false);
    await this.afAuth.auth.signOut();
    return this.router.navigate(['/']);
  }
  async sendEmailVerification() {
    await this.afAuth.auth.currentUser.sendEmailVerification()
  }
  private updateUserData(user, name) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`Users/${user.uid}`);

    const data = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName ? user.displayName : name,
      photoURL: user.photoURL,
      profile: null,
    };

    return userRef.set(data, {merge: true});
  }
}

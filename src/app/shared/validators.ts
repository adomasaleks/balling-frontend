import { AbstractControl } from '@angular/forms';
// fuction for validating maching passowrds
export function passwordValidator(control: AbstractControl) {
  if (control && (control.value !== null || control.value !== undefined)) {
    const conformPasswordValue = control.value;

    const passwordControl = control.root.get('password'); // magic is this
    if (passwordControl) {
      const passValue = passwordControl.value;
      if (passValue !== conformPasswordValue || passValue === '') {
        return {
          isError: true
        };
      }
    }
  }
  return null;
}

import { Injectable } from '@angular/core';
import {AngularFireAuth} from "@angular/fire/auth";
import {switchMap} from "rxjs/operators";
import {User} from "../core/models/user.model";
import {Observable, of} from "rxjs";
import {AngularFirestore} from "@angular/fire/firestore";

@Injectable()
export class Globals {
  user$: Observable<User>;
  uid: string;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
  ) {
    this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`Users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    ).subscribe( user => {
      this.uid = user.uid;
    });
  }
}
export function userId(): string {
return this.afAuth.uid;
}

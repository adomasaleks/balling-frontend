import {Component, OnInit} from '@angular/core';
import {NavbarComponent} from './modules/components/navbar/navbar.component';
import {AuthService} from './core/services/auth.service';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private auth: AuthService) {}
  title = 'balling';

}

